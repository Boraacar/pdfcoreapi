﻿using System;
using System.Collections.Generic;
using System.Text;
using embedPDF.Model;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace embedPDF.Model
{
    class SqlMethods
    {
        private string _connectionString;
        public SqlMethods(IConfiguration iconfiguration)
        {
            //_connectionString = iconfiguration.GetConnectionString("Default");
            _connectionString = "Server=192.168.1.8; Database=TimeBilling;User Id=sql2021user;Password=N#=YXG@7a-*-?3T$qM;MultipleActiveResultSets=true";
        }

        public DataTable GetReportDateNote(int CustCaseId)
        {
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand comm = new SqlCommand("ST_LAWERP_GET_CUST_CASE_GET_REPORT_DATE_NOTE", conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@CUSTCASEID", SqlDbType.Int);
                comm.Parameters["@CUSTCASEID"].Value = CustCaseId;


                conn.Open();

                using (comm)
                {
                    SqlDataAdapter da = new SqlDataAdapter(comm);
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }

        public DataTable getDavaOZellikleri(int CustCaseId)
        {
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand comm = new SqlCommand("SP_LAWERP_CUST_CASE_GET_CASE_PROPERTIES_FOR_REPORT_PDF", conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@CUSTCASEID", SqlDbType.Int);
                comm.Parameters["@CUSTCASEID"].Value = CustCaseId;

                conn.Open();

                using (comm)
                {
                    SqlDataAdapter da = new SqlDataAdapter(comm);
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }



    }
}

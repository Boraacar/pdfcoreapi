﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pdfCoreApi
{
    public class requestBody
    {
        public string accessToken { get; set; }
        public string scope { get; set; }
        public string authScope { get; set; }
        public string kcClient { get; set; }
        public string matterID { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string refreshToken { get; set; }
    }
}

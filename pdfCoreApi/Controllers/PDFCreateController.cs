﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using System.IO;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using Paragraph = iText.Layout.Element.Paragraph;
using System.Text;
using iText.IO;
using iText.Kernel.Pdf.Filespec;
using iText.Kernel.Pdf.Xobject;
using iText.Kernel.Pdf.Annot;
using iText.Kernel.Geom;
using iText.IO.Image;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Colors;
using iText.Kernel.Pdf.Action;
using iText.Layout.Properties;
using iText.Kernel.Font;
using iText.IO.Font.Constants;
using iText.Kernel.Pdf.Navigation;
using iText.Layout.Layout;
using Newtonsoft.Json;
using embedPDF.Model;
using System.Data;
using System.Globalization;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.AspNetCore.Http;
using iText.Layout.Borders;
using iText.Svg.Converter;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.Extensions.FileProviders;
using RestSharp;
using Newtonsoft.Json.Linq;
using MathNet;

namespace pdfCoreApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PDFCreateController : Controller
    {
        private static IConfiguration _iconfiguration;
        public static string DEST = /*PdfPath + */@"C:\igsi\web\SafahatReport.pdf";
        public static string DESTSERVER = @"E:\wwwroot\SafahatReportFile\SafahatReport.pdf";
        public static string DATA = @"C:\igsi\web\AddHelp.txt";
        //public static byte[] gotoIconLocal = System.IO.File.ReadAllBytes(@"C:\igsi\web\CustomerSystem\assets\img\Attach.png");
        public static byte[] gotoIconServer = System.IO.File.ReadAllBytes(@"E:\wwwroot\LawErp\portal\mecellem\CustomerSystem\assets\img\Attach.png");
        public static string UploadData = "";
        public static string SafahatUploadLocal = @"C:\igsi\web\";
        public static string SafahatUploadServer = @"E:\wwwroot\LawErp\ProjectsUploads\";
        public static string muhakematLogoLocal = @"C:\igsi\web\CustomerSystem\assets\img\muhakemat-logo.png";
        public static string muhakematLogoServer = @"E:\wwwroot\LawErp\portal\mecellem\CustomerSystem\assets\img\muhakemat-logo.png";
        public static string FileNameDATALocal = "";
        public static string FileNameDATAServer = "";
        public static string destfilename22 = "";
        //public IActionResult Index()
        //{
        //    return View();
        //}

        [HttpPost]
        [Route("DenemePost1")]
        public string DenemePost()
        {
            int CustCaseId = Convert.ToInt32(Request.Headers["CustCaseId"].ToString());
            string AccessToken = Request.Headers["AccessToken"].ToString();

            SqlMethods db = new SqlMethods(_iconfiguration);
            DataTable dtDateNote = db.GetReportDateNote(CustCaseId);
            DataTable davaOzellikleri = db.getDavaOZellikleri(CustCaseId);

            string matterID = "[" + davaOzellikleri.Rows[0][3].ToString() + "]:" + davaOzellikleri.Rows[0][4].ToString() + "[LIT]";


            bool auth = kcAuthorization(matterID, AccessToken);

            if (auth == true)
            {
                Random random = new Random();
                int num = random.Next(99999);
                string dest2 = davaOzellikleri.Rows[0][0].ToString();
                Regex rg = new Regex("[^a-zA-Z0-9]");
                //MatchCollection matchedAuthors = rg.Matches(dest2);
                string destfilename = rg.Replace(dest2, "_") + num;
                destfilename22 = destfilename;

                DEST = @"C:\igsi\web\" + destfilename + ".pdf";
                DESTSERVER = @"E:\wwwroot\SafahatReportFile\" + destfilename + ".pdf";



                PdfFont f = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD, "cp1254", true);
                PdfFont f3 = PdfFontFactory.CreateFont(StandardFonts.HELVETICA, "cp1254", true);
                PdfFont f2 = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN, "cp1254", true);
                PdfFont f1 = PdfFontFactory.CreateFont(StandardFonts.TIMES_BOLD, "cp1254", true);


                FileInfo file = new FileInfo(DESTSERVER);
                file.Directory.Create();

                PdfDocument pdfDoc = new PdfDocument(new PdfWriter(DESTSERVER));
                Document doc = new Document(pdfDoc, pdfDoc.GetDefaultPageSize(), false);
                PdfPage page = pdfDoc.AddNewPage();
                ImageData imageData = ImageDataFactory.Create(gotoIconServer);

                Paragraph p = new Paragraph().Add(new Text("\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0")).Add(new Text("Künye").SetFont(f).SetFontSize(7)).SetFontColor(ColorConstants.RED).SetWidth(110);
                Paragraph p1 = new Paragraph().Add( new Text("\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0")).Add(new Text("Safahat Kaydı").SetFont(f).SetFontSize(7)).SetFontColor(ColorConstants.RED).SetWidth(320);
                Paragraph p2 = new Paragraph().Add(new Text("\u00A0")).Add(new Text("Belgesi").SetFont(f).SetFontSize(7)).SetFontColor(ColorConstants.RED).SetWidth(30);
                Paragraph p4 = new Paragraph().Add(new Text("\u00A0")).Add(new Text("Sıra No").SetTextAlignment(TextAlignment.CENTER).SetFont(f).SetFontSize(7)).SetFontColor(ColorConstants.RED).SetWidth(40).SetTextAlignment(TextAlignment.CENTER);

                //Paragraph p3= new Paragraph().Add(new Text("").SetFont(f).SetFontSize(8)).SetFontColor(ColorConstants.RED).SetWidth(30);



                ImageData imageDatalogo = ImageDataFactory.Create(muhakematLogoServer);
                // Create layout image object and provide parameters. Page number = 1
                Image imagelogo = new Image(imageDatalogo).ScaleAbsolute(90, 25).SetFixedPosition(1, 460, 810);
                // This adds the image to the page
                doc.Add(imagelogo);
                //Border b1 = new Border();
                Color c1 = ColorConstants.GRAY;
                Paragraph Title = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f).SetFontSize(8);
                string linkmuhakemat = "https://muhakemat.mecellem.com/CustomerSystem/CustLitigationDetailsDev.aspx?projectId=" + davaOzellikleri.Rows[0][2].ToString() + "&CustCasesId=" + davaOzellikleri.Rows[0][1].ToString() + "";
                

                //var link = new Link(davaOzellikleri.Rows[0][0].ToString(), PdfAction.CreateURI(linkmuhakemat)).SetBorder(Border.NO_BORDER);
                var link = new Text(davaOzellikleri.Rows[0][0].ToString()).SetAction(PdfAction.CreateURI(linkmuhakemat)).SetTextAlignment(TextAlignment.CENTER).SetBorder(new RoundDotsBorder(c1,1))/*.SetBorderRadius(new BorderRadius(5))*/;
                Paragraph uyariNot = new Paragraph(new Text("*Safahate ait dökümanı görüntülemek için PDF önizleme programlarından birini kullanmalısınız.")).SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6);
                Paragraph uyari2 = new Paragraph(new Text("*İlgili Vaka/Dava Dosyasına ulaşmak için başlığa tıklayın.")).SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6);
                Title.Add(link);
                Title.Add("\n");
                Title.Add(uyariNot).Add("\n").Add(uyari2).SetPaddingLeft(67).SetPaddingRight(60).SetTextAlignment(TextAlignment.CENTER);
                doc.Add(Title).SetTextAlignment(TextAlignment.CENTER);

                int sayac = 0;
                int SayfaNum = 1;
                int sayacSayfaNum = 1;
                float tableHeight = 0;
                float tableHeighticTable = 0;
                float tableHeightFark = 0;
                float mypageSize = 735;

                Rectangle rect = new Rectangle(526, mypageSize, 14, 14);

                int safahatSayaci = 1;

                foreach (DataRow drSafahat in dtDateNote.Rows)
                {

                    FileNameDATALocal = drSafahat[25].ToString();
                    FileNameDATAServer = drSafahat[25].ToString();
                    Paragraph Ilgisi = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER).SetWidth(320);
                    Paragraph Baglantisi = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER).SetWidth(320);
                    Paragraph IlgiDongu = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER);
                    Paragraph BaglantiDongu = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER);

                    #region ilgi Alanı

                    List<string> Linkler = new List<string>();

                    if (!string.IsNullOrEmpty(drSafahat[21].ToString()))
                    {
                        string[] ilgilerveBaglantilari = drSafahat[21].ToString().Split(new string[] { "_***_" }, StringSplitOptions.None);

                        for (int i = 0; i < ilgilerveBaglantilari.Length; i++)
                        {
                            string[] ilgipart = ilgilerveBaglantilari[i].Split(new string[] { "_**_" }, StringSplitOptions.None);
                            if (ilgipart[2].ToString().Contains("Bağlantılı"))
                            {

                                string baglantiLinkstring = ilgipart[1].ToString();

                                if (string.IsNullOrEmpty(ilgipart[1].ToString()))
                                {
                                    var linkBaglanti = new Text(ilgipart[0].ToString()).SetUnderline();
                                    BaglantiDongu.Add(linkBaglanti);
                                }
                                else
                                {
                                    //var linkBaglanti = new Link(ilgipart[0].ToString(), PdfAction.CreateURI(baglantiLinkstring)).SetBorder(Border.NO_BORDER);
                                    var linkBaglanti = new Text(ilgipart[0].ToString()).SetAction(PdfAction.CreateURI(baglantiLinkstring)).SetUnderline(); ;
                                    BaglantiDongu.Add(linkBaglanti);
                                }

                                BaglantiDongu.Add("\n");


                            }

                            else
                            {
                                string ilgiLinkstring = ilgipart[1].ToString();

                                if (string.IsNullOrEmpty(ilgipart[1].ToString()))
                                {
                                    var linkIlgi = new Text(ilgipart[0].ToString());
                                    IlgiDongu.Add(linkIlgi);
                                }

                                else
                                {
                                    //var linkIlgi = new Link(ilgipart[0].ToString(), PdfAction.CreateURI(ilgiLinkstring)).SetBorder(Border.NO_BORDER);
                                    var linkIlgi = new Text(ilgipart[0].ToString()).SetAction(PdfAction.CreateURI(ilgiLinkstring));
                                    IlgiDongu.Add(linkIlgi);
                                }

                                IlgiDongu.Add("\n");
                            }
                        }
                        Ilgisi.Add(IlgiDongu);
                        Baglantisi.Add(BaglantiDongu);

                    }


                    #endregion


                    FileInfo file123 = new FileInfo(SafahatUploadServer + "Bulunamayanbir.pdf");
                    //UploadData = SafahatUploadServer + "bos.pdf";
                    if (!string.IsNullOrEmpty(drSafahat[24].ToString()) && drSafahat[24].ToString() != "0")
                    {
                        UploadData = SafahatUploadServer + drSafahat[24].ToString();
                        file123 = new FileInfo(UploadData);
                        //UploadData = SafahatUploadLocal + "AddHelp.txt";
                    }

                    else
                    {
                        UploadData = "";
                    }

                    Table table = new Table(UnitValue.CreatePercentArray(4)).UseAllAvailableWidth();
                    table.AddHeaderCell(p4).SetWidth(40);
                    table.AddHeaderCell(p).SetWidth(110);
                    table.AddHeaderCell(p1).SetWidth(320);
                    table.AddHeaderCell(p2).SetWidth(30);

                    for (int i = 0; i < 4; i++)
                    {
                        Cell mycell = new Cell();

                        if (i == 3) // Annotation eklenecek cell. Boş bırakılıyor. Daha sonra annotation konulması için
                        {
                            table.AddCell("");


                        }
                        else if (i == 2) // Safahat Bilgilerinin yazıldığı cell
                        {

                            Paragraph IlgiAlaka = new Paragraph();

                            Paragraph Konusu = new Paragraph().Add(new Text("Konusu:\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);

                            Paragraph DahiliNot = new Paragraph().Add(new Text(" Dahili Not:\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph Konusuic = new Paragraph().Add(new Text(drSafahat[3].ToString() + " \n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(320);

                            Paragraph DahiliNotic = new Paragraph().Add(new Text(drSafahat[9].ToString() + " \n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(320);
                            Paragraph Ilgiler = new Paragraph().Add(new Text("İlgi:").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            //Paragraph Ilgileric = new Paragraph().Add(Ilgisi).SetWidth(350);

                            Paragraph Baglantilar = new Paragraph().Add(new Text("Bağlantılı Belgeler:").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph BaglantilaricBosluk = new Paragraph().Add(new Text("Bağlantılı Belgeler").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            //Paragraph Baglantilaric = new Paragraph().Add(Baglantisi).SetWidth(350);

                            //IlgiAlaka.Add(Ilgiler.Add("\n").Add(Ilgileric)).Add("\n").Add(Baglantilar).Add("\n").Add(Baglantilaric);
                            //IlgiAlaka.Add(Ilgiler.Add("\n").Add(Ilgileric.Add("\n").Add(Baglantilar.Add("\n").Add(Baglantilaric))));

                            Konusu.Add(Konusuic.Add(DahiliNot.Add(DahiliNotic.Add(Ilgiler.Add("\n").Add(Ilgisi.Add("\n").Add(Baglantilar.Add("\n").Add(Baglantisi)))))));
                            //Konusu.Add("\n");
                            //Konusu.Add(Baglantilar.Add(Baglantilaric));


                            table.AddCell(Konusu).SetTextAlignment(TextAlignment.JUSTIFIED);

                        }

                        else if (i == 1)// Tarih yazılacak cell
                        {
                            IFormatProvider culture = new CultureInfo("tr-TR", true);

                            Paragraph Tarih = new Paragraph().Add(new Text("Tarih:\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph Tarihic = new Paragraph().Add(new Text(DateTime.Parse(drSafahat[2].ToString()).ToString("dd.MM.yyyy hh:mm", culture) + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph Kimden = new Paragraph().Add(new Text("Kimden:\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph Kimdenic = new Paragraph().Add(new Text(drSafahat[12].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(110);
                            Paragraph Kime = new Paragraph().Add(new Text("Kime:\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph Kimeic = new Paragraph().Add(new Text(drSafahat[13].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(110);
                            Paragraph Niteligi = new Paragraph().Add(new Text("Niteliği:\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph Niteligiic = new Paragraph().Add(new Text(drSafahat[15].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph BelgeTuru = new Paragraph().Add(new Text("Belge Türü:\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                            Paragraph BelgeTuruic = new Paragraph().Add(new Text(drSafahat[5].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(110);

                            Tarih.Add(Tarihic.Add(Kimden.Add(Kimdenic.Add(Kime.Add(Kimeic.Add(Niteligi.Add(Niteligiic.Add(BelgeTuru.Add(BelgeTuruic)))))))));

                            table.AddCell(Tarih).SetTextAlignment(TextAlignment.JUSTIFIED);
                        }

                        else
                        {
                            
                            Paragraph Numarabosluk = new Paragraph().Add(new Text("\n\n\n\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(8).SetTextAlignment(TextAlignment.CENTER);
                            //Paragraph bosluk = new Paragraph().Add(new Text("\u00A0\u00A0").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(8).SetTextAlignment(TextAlignment.CENTER);
                            Paragraph Numara = new Paragraph().Add(new Text("\u00A0\u00A0")).Add(new Text(safahatSayaci.ToString()).SetFont(f1).SetFontSize(8)).SetFontColor(ColorConstants.BLACK).SetWidth(40).SetTextAlignment(TextAlignment.CENTER);
                            Numarabosluk.Add(Numara).SetTextAlignment(TextAlignment.CENTER);
                            table.AddCell(Numarabosluk).SetTextAlignment(TextAlignment.CENTER);
                        }
                    }
                    LayoutResult layoutResult25 = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                    tableHeighticTable = tableHeighticTable + layoutResult25.GetOccupiedArea().GetBBox().GetHeight();

                    //Table dökümana ekleniyor.

                    if (tableHeighticTable > 700)
                    {
                        //page = pdfDoc.AddNewPage();

                        doc.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        //sayac = 0;
                        tableHeighticTable = layoutResult25.GetOccupiedArea().GetBBox().GetHeight();
                        tableHeight = 0;
                        SayfaNum = pdfDoc.GetNumberOfPages();
                        page = pdfDoc.GetPage(SayfaNum);



                        doc.Add(table);
                        sayac = 0;

                    }
                    //page = pdfDoc.GetPage(SayfaNum);
                    else
                    {
                        doc.Add(table);
                    }



                    //Annnotation'lar ekleniyor. Eğer ilk table'ın annotation'ı ise koordinatı bellidir. Sonrakiler table height'ına göre aşağıya doğru uzuyor.
                    if (SayfaNum == 1)
                    {
                        if (sayac == 0)
                        {
                            if (!string.IsNullOrEmpty(drSafahat[11].ToString()))
                            {
                                ImageData imageFlag = ImageDataFactory.Create(flagfilePath(drSafahat[11].ToString()));
                                Image Flagimage = new Image(imageFlag).ScaleAbsolute(18, 18).SetFixedPosition(SayfaNum, 526, mypageSize - 113);
                                doc.Add(Flagimage);

                            }

                            if (!string.IsNullOrEmpty(UploadData) && file123.Exists)
                            {
                                //FileInfo file123123 = new FileInfo(UploadData);
                                ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect.SetY(mypageSize - 53));
                            }
                            LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                            tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                            //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                        }

                        else
                        {
                            if (!string.IsNullOrEmpty(drSafahat[11].ToString()))
                            {
                                ImageData imageFlag = ImageDataFactory.Create(flagfilePath(drSafahat[11].ToString()));
                                Image Flagimage = new Image(imageFlag).ScaleAbsolute(18, 18).SetFixedPosition(SayfaNum, 526, mypageSize - 113 - tableHeight);
                                doc.Add(Flagimage);

                            }

                            if (!string.IsNullOrEmpty(UploadData) && file123.Exists)
                            {
                                rect = new Rectangle(526, mypageSize - 53 - tableHeight, 14, 14);
                                ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect);
                            }

                            LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                            tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                            //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();

                        }
                    }

                    else
                    {
                        if (sayac == 0)
                        {

                            if (!string.IsNullOrEmpty(drSafahat[11].ToString()))
                            {
                                ImageData imageFlag = ImageDataFactory.Create(flagfilePath(drSafahat[11].ToString()));
                                Image Flagimage = new Image(imageFlag).ScaleAbsolute(18, 18).SetFixedPosition(SayfaNum, 526, mypageSize - 60);
                                doc.Add(Flagimage);

                            }

                            if (!string.IsNullOrEmpty(UploadData) && file123.Exists)
                            {
                                ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect.SetY(mypageSize));
                            }
                            LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                            tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                            //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                        }

                        else
                        {

                            if (!string.IsNullOrEmpty(drSafahat[11].ToString()))
                            {
                                ImageData imageFlag = ImageDataFactory.Create(flagfilePath(drSafahat[11].ToString()));
                                Image Flagimage = new Image(imageFlag).ScaleAbsolute(18, 18).SetFixedPosition(SayfaNum, 526, mypageSize - 60 - tableHeight);
                                doc.Add(Flagimage);

                            }

                            if (!string.IsNullOrEmpty(UploadData) && file123.Exists)
                            {
                                rect = new Rectangle(526, mypageSize - tableHeight, 14, 14);
                                ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect);
                            }

                            LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                            tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                            //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();

                        }
                    }


                    sayac++;

                    safahatSayaci = safahatSayaci + 1;

                    SayfaNum = pdfDoc.GetNumberOfPages();
                }

                doc.Close();
                pdfDoc.Close();

                return destfilename22 + ".pdf";
                ////string denemefilePath = @"C:\igsi\web\HB___PROJECT_INTEGRITY___1_20_cv_02648_DLC__SAFAHAT_KAYITLARI.pdf";
                //string denemefilePath = @"C:\igsi\web\";

                //IFileProvider provider = new PhysicalFileProvider(denemefilePath);
                //IFileInfo fileInfo = provider.GetFileInfo(destfilename22 + ".pdf");
                //var readStream = fileInfo.CreateReadStream();

                ////return File(denemefilePath, "text/plain");
                //return File(readStream, "application/pdf", destfilename22 + ".pdf");
            }

            else
            {
                //izin yok
                var readStream = "";
                return "";

            }

        }


        [HttpGet]
        [Route("DenemeGet1")]
        public string DenemeGet1()
        {
            return "calisiyor";
        }

        protected static void ManipulatePdf3(PdfDocument pdfDoc, Document doc, PdfPage page, ImageData imageData, string embededFile, Rectangle rect)
        {

            string embeddedFileName = embededFile;
            PdfFileSpec fileSpec = PdfFileSpec.CreateEmbeddedFileSpec(pdfDoc, embeddedFileName, null, embeddedFileName, null);

            PdfAnnotation attachment = new PdfFileAttachmentAnnotation(rect, fileSpec);
            attachment.SetContents(FileNameDATAServer);
            //attachment.SetContents("Belgeye Git");
            //attachment.SetBorder(new PdfAnnotationBorder(5, 5, 5));
            //attachment.SetTitle(new PdfString(embeddedFileName, null));

            PdfFormXObject xObject = new PdfFormXObject(rect);
            //PdfCanvas canvas = new PdfCanvas(xObject, pdfDoc);
            PdfCanvas canvas = new PdfCanvas(page);
            canvas.AddImageFittedIntoRectangle(imageData, rect, false);
            attachment.SetNormalAppearance(xObject.GetPdfObject());
            page.AddAnnotation(attachment);

            //Image myImage = new Image(xObject);


            //Table table1 = new Table(UnitValue.CreatePercentArray(5)).UseAllAvailableWidth();

            //Image img = new Image(ImageDataFactory.Create(MYIMG));

            //table1.AddCell(img);
            //table1.AddCell("xxxx").SetAction(PdfAction.CreateURI("https://www.hurriyet.com.tr"));
            //table1.AddCell("xxxx").SetAction(PdfAction.CreateGoToE(fileSpec, null, true, PdfTarget.CreateChildTarget(embededFile)));

            //doc.Add(table1);

        }

        public static string flagfilePath(string onemDerece)
        {
            if (onemDerece == "1")
            {
                return @"E:\wwwroot\LawErp\portal\mecellem\CustomerSystem\assets\img\redflag.png";
            }

            else if (onemDerece == "2")
            {
                return @"E:\wwwroot\LawErp\portal\mecellem\CustomerSystem\assets\img\yellowflag.png";
            }

            else
            {
                return @"E:\wwwroot\LawErp\portal\mecellem\CustomerSystem\assets\img\greenflag.png";
            }
        }



        public bool kcAuthorization(string matterID, string ACCESSTOKEN)
        {
            var mytoken = ACCESSTOKEN;

            requestBody body = new requestBody();
            body.accessToken = mytoken;
            body.scope = "INDIRME";
            body.kcClient = "IGSI";
            body.matterID = matterID;

            var authObj = RESTPostRequest("authorizationByAccessToken", body);

            //Users user = JsonConvert.DeserializeObject<Users>(authObj.ToString());
            if (Convert.ToBoolean(authObj.SelectToken("authorization").ToString()) == true)
            {
                return true;
            }

            else
            {
                return false;
            }


        }


        public static JObject RESTPostRequest(string kcResourceUrl, requestBody body)
        {
            string myBody = JsonConvert.SerializeObject(body);

            string kcAuthUrl = "https://kcssoapi.mecellem.com/KCAPI/authorizationByAccessToken/";
            var client = new RestClient(kcAuthUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.Parameters.Clear();
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddParameter("application/json", myBody, ParameterType.RequestBody);
            JObject rootcatch = null;

            try
            {
                IRestResponse response = client.Execute(request);

                JObject root = JObject.Parse(response.Content);
                return root;
            }
            catch (Exception e)
            {

            }
            return rootcatch;

        }




        public void ProcessRequest(HttpContext context)
        {
            //İndirme İşlemi

            //string denemeBegleAdi = "Belge1";
            //string denemefilePath = @"C:\igsi\web\Belge1.pdf";
            //string denemefileExtencion = ".pdf";


            //FileInfo fileeee = new FileInfo(@"C:\igsi\web\Belge1.pdf");


            //if (fileeee.Exists)
            //{
            //    context.Response.ContentType = "application/octet-stream";
            //    context.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}",/* DBFileAddedDate + " " + DBFileTypeText + " " + */denemeBegleAdi.Replace('_', ' ') + denemefileExtencion));
            //    // Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", DBFileName.Replace("İ", "I").Replace("Ğ", "G").Replace("Ü", "U").Replace("Ş", "S").Replace("Ç", "C").Replace("Ö", "O").Replace("ı", "i").Replace("ş", "s").Replace("ğ", "g").Replace("ç", "c").Replace("ü", "u").Replace(",", "_")));
            //    context.Response.TransmitFile(denemefilePath.ToString());
            //    context.Response.End();
            //    context.Response.ClearHeaders();
            //    context.Response.ClearContent();
            //    context.Response.Clear();
            //}

            //else
            //{
            //    context.Response.Redirect("AccessDenied.aspx");
            //}
        }

    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using System.IO;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using Paragraph = iText.Layout.Element.Paragraph;
using System.Text;
using iText.IO;
using iText.Kernel.Pdf.Filespec;
using iText.Kernel.Pdf.Xobject;
using iText.Kernel.Pdf.Annot;
using iText.Kernel.Geom;
using iText.IO.Image;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Colors;
using iText.Kernel.Pdf.Action;
using iText.Layout.Properties;
using iText.Kernel.Font;
using iText.IO.Font.Constants;
using iText.Kernel.Pdf.Navigation;
using iText.Layout.Layout;
using Newtonsoft.Json;
using embedPDF.Model;
using System.Data;
using System.Globalization;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.AspNetCore.Http;
using iText.Layout.Borders;
using iText.Svg.Converter;
using System.Text.RegularExpressions;

namespace pdfCoreApi
{
    public class Program
    {
        public static string FilePath;
        public static string PdfPath;

        public Program(IConfiguration iconfiguration)
        {
            FilePath = iconfiguration.GetConnectionString("PathLocal");
            PdfPath = iconfiguration.GetConnectionString("PathPdfLocal");
        }



        public static string DEST = /*PdfPath + */@"C:\igsi\web\SafahatReport.pdf";
        public static string DESTSERVER = @"E:\wwwroot\LawErp\portal\mecellem\SafahatReportFile\SafahatReport.pdf";
        public static string DATA = @"C:\igsi\web\AddHelp.txt";
        public static byte[] gotoIconLocal = File.ReadAllBytes(@"C:\igsi\web\CustomerSystem\assets\img\Attach.png");
        //public static byte[] gotoIconServer = File.ReadAllBytes(@"E:\wwwroot\LawErp\portal\mecellem\CustomerSystem\assets\img\Attach.png");
        public static string UploadData = "";
        public static string SafahatUploadLocal = @"C:\igsi\web\";
        public static string SafahatUploadServer = @"E:\wwwroot\LawErp\ProjectsUploads\";
        public static string muhakematLogoLocal = @"C:\igsi\web\CustomerSystem\assets\img\muhakemat-logo.png";
        public static string muhakematLogoServer = @"E:\wwwroot\LawErp\portal\mecellem\CustomerSystem\assets\img\muhakemat-logo.png";
        public static string FileNameDATALocal = "";
        public static string FileNameDATAServer = "";


        private static IConfiguration _iconfiguration;
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            GetAppSettingsFile();
            ReportDateNote();
        }

        #region configi uyandırmak için başlangıç methodu
        static void GetAppSettingsFile()
        {
            var builder = new ConfigurationBuilder()
                                 .SetBasePath(Directory.GetCurrentDirectory())
                                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            _iconfiguration = builder.Build();
        }
        #endregion

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });


        static void ReportDateNote()
        {
            SqlMethods db = new SqlMethods(_iconfiguration);
            DataTable dtDateNote = db.GetReportDateNote(1079938);
            DataTable davaOzellikleri = db.getDavaOZellikleri(1079938);

            string dest2 = davaOzellikleri.Rows[0][0].ToString();
            Regex rg = new Regex("[^a-zA-Z0-9]");
            //MatchCollection matchedAuthors = rg.Matches(dest2);
            string destfilename = rg.Replace(dest2, "_");

            DEST = @"C:\igsi\web\" + destfilename + ".pdf";
            DESTSERVER = @"E:\wwwroot\LawErp\portal\mecellem\SafahatReportFile" + destfilename + ".pdf";



            PdfFont f = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD, "cp1254", true);
            PdfFont f2 = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN, "cp1254", true);
            PdfFont f1 = PdfFontFactory.CreateFont(StandardFonts.TIMES_BOLD, "cp1254", true);


            FileInfo file = new FileInfo(DEST);
            file.Directory.Create();

            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(DEST));
            Document doc = new Document(pdfDoc, pdfDoc.GetDefaultPageSize(), false);
            PdfPage page = pdfDoc.AddNewPage();
            ImageData imageData = ImageDataFactory.Create(gotoIconLocal);

            Paragraph p = new Paragraph().Add(new Text("Künye").SetFont(f).SetFontSize(8)).SetFontColor(ColorConstants.RED).SetWidth(120);
            Paragraph p1 = new Paragraph().Add(new Text("Safahat Kaydı").SetFont(f).SetFontSize(8)).SetFontColor(ColorConstants.RED).SetWidth(350);
            Paragraph p2 = new Paragraph().Add(new Text("Belgesi").SetFont(f).SetFontSize(8)).SetFontColor(ColorConstants.RED).SetWidth(30);

            //Paragraph p3= new Paragraph().Add(new Text("").SetFont(f).SetFontSize(8)).SetFontColor(ColorConstants.RED).SetWidth(30);



            ImageData imageDatalogo = ImageDataFactory.Create(muhakematLogoLocal);
            // Create layout image object and provide parameters. Page number = 1
            Image imagelogo = new Image(imageDatalogo).ScaleAbsolute(90, 25).SetFixedPosition(1, 460, 810);
            // This adds the image to the page
            doc.Add(imagelogo);

            Paragraph Title = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f).SetFontSize(9).SetBorder(Border.NO_BORDER);
            string linkmuhakemat = "https://muhakemat.mecellem.com/CustomerSystem/CustLitigationDetailsDev.aspx?projectId=" + davaOzellikleri.Rows[0][2].ToString() + "&CustCasesId=" + davaOzellikleri.Rows[0][1].ToString() + "";

            //var link = new Link(davaOzellikleri.Rows[0][0].ToString(), PdfAction.CreateURI(linkmuhakemat)).SetBorder(Border.NO_BORDER);
            var link = new Text(davaOzellikleri.Rows[0][0].ToString()).SetAction(PdfAction.CreateURI(linkmuhakemat));
            link.SetBorder(Border.NO_BORDER);
            Title.SetBorder(Border.NO_BORDER);
            Title.Add(link);
            doc.Add(Title);

            int sayac = 0;
            int SayfaNum = 1;
            int sayacSayfaNum = 1;
            float tableHeight = 0;
            float tableHeighticTable = 0;
            float tableHeightFark = 0;
            float mypageSize = 735;

            Rectangle rect = new Rectangle(526, mypageSize, 14, 14);



            foreach (DataRow drSafahat in dtDateNote.Rows)
            {
                FileNameDATALocal = drSafahat[25].ToString();
                FileNameDATAServer = drSafahat[25].ToString();
                Paragraph Ilgisi = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER).SetWidth(350);
                Paragraph Baglantisi = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER).SetWidth(350);
                Paragraph IlgiDongu = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER);
                Paragraph BaglantiDongu = new Paragraph().SetFontColor(ColorConstants.GRAY).SetFont(f2).SetFontSize(6).SetBorder(Border.NO_BORDER);

                #region ilgi Alanı

                List<string> Linkler = new List<string>();

                if (!string.IsNullOrEmpty(drSafahat[21].ToString()))
                {
                    string[] ilgilerveBaglantilari = drSafahat[21].ToString().Split(new string[] { "_***_" }, StringSplitOptions.None);

                    for (int i = 0; i < ilgilerveBaglantilari.Length; i++)
                    {
                        string[] ilgipart = ilgilerveBaglantilari[i].Split(new string[] { "_**_" }, StringSplitOptions.None);
                        if (ilgipart[2].ToString().Contains("Bağlantılı"))
                        {

                            string baglantiLinkstring = ilgipart[1].ToString();

                            if (string.IsNullOrEmpty(ilgipart[1].ToString()))
                            {
                                var linkBaglanti = new Text(ilgipart[0].ToString()).SetUnderline();
                                BaglantiDongu.Add(linkBaglanti);
                            }
                            else
                            {
                                //var linkBaglanti = new Link(ilgipart[0].ToString(), PdfAction.CreateURI(baglantiLinkstring)).SetBorder(Border.NO_BORDER);
                                var linkBaglanti = new Text(ilgipart[0].ToString()).SetAction(PdfAction.CreateURI(baglantiLinkstring)).SetUnderline(); ;
                                BaglantiDongu.Add(linkBaglanti);
                            }

                            BaglantiDongu.Add("\n");


                        }

                        else
                        {
                            string ilgiLinkstring = ilgipart[1].ToString();

                            if (string.IsNullOrEmpty(ilgipart[1].ToString()))
                            {
                                var linkIlgi = new Text(ilgipart[0].ToString());
                                IlgiDongu.Add(linkIlgi);
                            }

                            else
                            {
                                //var linkIlgi = new Link(ilgipart[0].ToString(), PdfAction.CreateURI(ilgiLinkstring)).SetBorder(Border.NO_BORDER);
                                var linkIlgi = new Text(ilgipart[0].ToString()).SetAction(PdfAction.CreateURI(ilgiLinkstring));
                                IlgiDongu.Add(linkIlgi);
                            }

                            IlgiDongu.Add("\n");
                        }
                    }
                    Ilgisi.Add(IlgiDongu);
                    Baglantisi.Add(BaglantiDongu);

                }


                #endregion



                if (!string.IsNullOrEmpty(drSafahat[24].ToString()) && drSafahat[24].ToString() != "0")
                {
                    UploadData = SafahatUploadLocal + drSafahat[24].ToString();
                    UploadData = SafahatUploadLocal + "AddHelp.txt";
                }

                else
                {
                    UploadData = "";
                }

                Table table = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();

                table.AddHeaderCell(p).SetWidth(120);
                table.AddHeaderCell(p1).SetWidth(350);
                table.AddHeaderCell(p2).SetWidth(30);

                for (int i = 0; i < 3; i++)
                {
                    Cell mycell = new Cell();

                    if (i == 2) // Annotation eklenecek cell. Boş bırakılıyor. Daha sonra annotation konulması için
                    {
                        table.AddCell("");


                    }
                    else if (i == 1) // Safahat Bilgilerinin yazıldığı cell
                    {

                        Paragraph IlgiAlaka = new Paragraph();

                        Paragraph Konusu = new Paragraph().Add(new Text("Konusu\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);

                        Paragraph DahiliNot = new Paragraph().Add(new Text(" Dahili Not\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph Konusuic = new Paragraph().Add(new Text(drSafahat[3].ToString() + " \n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(350);

                        Paragraph DahiliNotic = new Paragraph().Add(new Text(drSafahat[9].ToString() + " \n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(350);
                        Paragraph Ilgiler = new Paragraph().Add(new Text("İlgi").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        //Paragraph Ilgileric = new Paragraph().Add(Ilgisi).SetWidth(350);

                        Paragraph Baglantilar = new Paragraph().Add(new Text("Bağlantılı Belgeler").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph BaglantilaricBosluk = new Paragraph().Add(new Text("Bağlantılı Belgeler").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        //Paragraph Baglantilaric = new Paragraph().Add(Baglantisi).SetWidth(350);

                        //IlgiAlaka.Add(Ilgiler.Add("\n").Add(Ilgileric)).Add("\n").Add(Baglantilar).Add("\n").Add(Baglantilaric);
                        //IlgiAlaka.Add(Ilgiler.Add("\n").Add(Ilgileric.Add("\n").Add(Baglantilar.Add("\n").Add(Baglantilaric))));

                        Konusu.Add(Konusuic.Add(DahiliNot.Add(DahiliNotic.Add(Ilgiler.Add("\n").Add(Ilgisi.Add("\n").Add(Baglantilar.Add("\n").Add(Baglantisi)))))));
                        //Konusu.Add("\n");
                        //Konusu.Add(Baglantilar.Add(Baglantilaric));


                        table.AddCell(Konusu);

                    }

                    else // Tarih yazılacak cell
                    {
                        IFormatProvider culture = new CultureInfo("tr-TR", true);

                        Paragraph Tarih = new Paragraph().Add(new Text("Tarih\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph Tarihic = new Paragraph().Add(new Text(DateTime.Parse(drSafahat[2].ToString()).ToString("dd.MM.yyyy hh:mm", culture) + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph Kimden = new Paragraph().Add(new Text("Kimden\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph Kimdenic = new Paragraph().Add(new Text(drSafahat[12].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(120);
                        Paragraph Kime = new Paragraph().Add(new Text("Kime\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph Kimeic = new Paragraph().Add(new Text(drSafahat[13].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(120);
                        Paragraph Niteligi = new Paragraph().Add(new Text("Niteliği\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph Niteligiic = new Paragraph().Add(new Text(drSafahat[15].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph BelgeTuru = new Paragraph().Add(new Text("Belge Türü\n").SetFont(f).SetFontSize(6)).SetFontColor(ColorConstants.BLACK);
                        Paragraph BelgeTuruic = new Paragraph().Add(new Text(drSafahat[5].ToString() + "\n\n").SetFont(f2).SetFontSize(6)).SetFontColor(ColorConstants.BLACK).SetWidth(120);

                        Tarih.Add(Tarihic.Add(Kimden.Add(Kimdenic.Add(Kime.Add(Kimeic.Add(Niteligi.Add(Niteligiic.Add(BelgeTuru.Add(BelgeTuruic)))))))));

                        table.AddCell(Tarih);
                    }
                }
                LayoutResult layoutResult25 = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                tableHeighticTable = tableHeighticTable + layoutResult25.GetOccupiedArea().GetBBox().GetHeight();

                //Table dökümana ekleniyor.

                if (tableHeighticTable > 700)
                {
                    //page = pdfDoc.AddNewPage();

                    doc.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                    //sayac = 0;
                    tableHeighticTable = layoutResult25.GetOccupiedArea().GetBBox().GetHeight();
                    tableHeight = 0;
                    SayfaNum = pdfDoc.GetNumberOfPages();
                    page = pdfDoc.GetPage(SayfaNum);



                    doc.Add(table);
                    sayac = 0;

                }
                //page = pdfDoc.GetPage(SayfaNum);
                else
                {
                    doc.Add(table);
                }



                //Annnotation'lar ekleniyor. Eğer ilk table'ın annotation'ı ise koordinatı bellidir. Sonrakiler table height'ına göre aşağıya doğru uzuyor.
                if (SayfaNum == 1)
                {
                    if (sayac == 0)
                    {
                        if (!string.IsNullOrEmpty(UploadData))
                        {
                            ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect.SetY(mypageSize - 23));
                        }
                        LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                        tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                        //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                    }

                    else
                    {
                        if (!string.IsNullOrEmpty(UploadData))
                        {
                            rect = new Rectangle(526, mypageSize - 23 - tableHeight, 14, 14);
                            ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect);
                        }

                        LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                        tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                        //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();

                    }
                }

                else
                {
                    if (sayac == 0)
                    {
                        if (!string.IsNullOrEmpty(UploadData))
                        {
                            ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect.SetY(mypageSize));
                        }
                        LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                        tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                        //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                    }

                    else
                    {
                        if (!string.IsNullOrEmpty(UploadData))
                        {
                            rect = new Rectangle(526, mypageSize - tableHeight, 14, 14);
                            ManipulatePdf3(pdfDoc, doc, page, imageData, UploadData, rect);
                        }

                        LayoutResult layoutResult = table.CreateRendererSubTree().SetParent(doc.GetRenderer()).Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, 400, 10000.0F))));
                        tableHeight = tableHeight + layoutResult.GetOccupiedArea().GetBBox().GetHeight();
                        //tableHeighticTable = tableHeighticTable + layoutResult.GetOccupiedArea().GetBBox().GetHeight();

                    }
                }


                sayac++;

                SayfaNum = pdfDoc.GetNumberOfPages();
            }

            doc.Close();
            pdfDoc.Close();
        }

        protected static void ManipulatePdf3(PdfDocument pdfDoc, Document doc, PdfPage page, ImageData imageData, string embededFile, Rectangle rect)
        {

            string embeddedFileName = embededFile;
            PdfFileSpec fileSpec = PdfFileSpec.CreateEmbeddedFileSpec(pdfDoc, embeddedFileName, null, embeddedFileName, null);

            PdfAnnotation attachment = new PdfFileAttachmentAnnotation(rect, fileSpec);
            attachment.SetContents(FileNameDATALocal);
            //attachment.SetContents("Belgeye Git");
            //attachment.SetBorder(new PdfAnnotationBorder(5, 5, 5));
            //attachment.SetTitle(new PdfString(embeddedFileName, null));

            PdfFormXObject xObject = new PdfFormXObject(rect);
            //PdfCanvas canvas = new PdfCanvas(xObject, pdfDoc);
            PdfCanvas canvas = new PdfCanvas(page);
            canvas.AddImageFittedIntoRectangle(imageData, rect, false);
            attachment.SetNormalAppearance(xObject.GetPdfObject());
            page.AddAnnotation(attachment);

            //Image myImage = new Image(xObject);


            //Table table1 = new Table(UnitValue.CreatePercentArray(5)).UseAllAvailableWidth();

            //Image img = new Image(ImageDataFactory.Create(MYIMG));

            //table1.AddCell(img);
            //table1.AddCell("xxxx").SetAction(PdfAction.CreateURI("https://www.hurriyet.com.tr"));
            //table1.AddCell("xxxx").SetAction(PdfAction.CreateGoToE(fileSpec, null, true, PdfTarget.CreateChildTarget(embededFile)));

            //doc.Add(table1);

        }

    }
}
